/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swgenerator_fxml;

import OPCTreeHelper.ParamReplacer;
import OPCTreeHelper.ParamSearcher;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import OPCTreeParams.AddressCreator;
import OPCTreeParams.AttributeCreator;
import OPCTreeHelper.Param;
import OPCTreeParams.TemplateAddress;
import OPCTreeParams.TemplateAddressCreator;
import java.awt.Desktop;
import javafx.scene.control.Label;

/**
 *
 * @author NovikovLU
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private ListView paramsListView;
    @FXML
    private ChoiceBox templateChoiseBox;
    @FXML
    private Button scanButton;
    @FXML
    private Button generateButton;
    @FXML
    private TextField baseAddressField;
    @FXML
    private TextField outputFileNameField;
    @FXML
    private Button chooseFileButton;
    @FXML
    private Button openDirectoryButton;
    @FXML
    private Label sourceFileNameLabel;
    @FXML
    private Button replaceAddressButton;
    
    public static String TemplatesPath = "templates/";
    public static String OutputFilesPath = "outputFiles/";
    private String Source = "";
    private Map <String, Param> Attributes;
    private Map <String, Param> Addresses;
    private Map <String, Param> Params;
    private File SourceFile;
    
    @FXML
    private void handleScanButtonAction() {
        try {
            String file = (String)templateChoiseBox.getValue();
            if(file != null)
            {
                Source = FileToString(TemplatesPath + file);

                ParamSearcher attributeSearcher = new ParamSearcher(Source);
                AttributeCreator attributeCreator = new AttributeCreator();
                Attributes = attributeSearcher.SearchParams(attributeCreator);
                String[] attributesKeys = getMapStringKeys(Attributes);
                paramsListView.setItems(FXCollections.observableArrayList(attributesKeys));
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
    
    @FXML
    private void handleGenerateButtonAction() {
        Alert warningAlert = new Alert(Alert.AlertType.WARNING);
        Alert successAlert = new Alert(Alert.AlertType.INFORMATION);
        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        Optional<ButtonType> result;
        if(Source!="")
        {
            setAttributesValues();
            if(validateAddress())
            {
                int baseAddress = getBaseAddress();
                AddressCreator addressCreator = new AddressCreator();
                addressCreator.setBaseAddress(baseAddress);
                ParamSearcher addressSearcher = new ParamSearcher(Source);
                Addresses = addressSearcher.SearchParams(addressCreator);
                Params.putAll(Attributes);
                Params.putAll(Addresses);

                ParamReplacer paramReplacer = new ParamReplacer(Source, Params);
                String txt = paramReplacer.ReplaceParams();
                
                int status = outputInFile(txt);
                switch(status)
                {
                    case 1:
                        successAlert.setContentText("File generate successfuly");
                        result = successAlert.showAndWait();
                        break;
                    case -1:
                        warningAlert.setContentText("File can't be created");
                        result = warningAlert.showAndWait();
                        break;
                    case 0:
                        errorAlert.setContentText("Unsupported encoding or IOException. Look in console.");
                        result = errorAlert.showAndWait();
                        break;
                    case 2:
                        warningAlert.setContentText("Please, enter filename");
                        result = warningAlert.showAndWait();
                        break;
                }
            }
            else
            {
                warningAlert.setContentText("Incorrect base address");
                result = warningAlert.showAndWait();
            }
        }else
        {
            warningAlert.setContentText("Source file is empty. Chiose file and press 'Scan' button");
            result = warningAlert.showAndWait();
        }
    }
    
    @FXML
    private void handleChooseFile()
    {
        FileChooser fileChooser = new FileChooser();
        SourceFile = fileChooser.showOpenDialog(chooseFileButton.getScene().getWindow());
        sourceFileNameLabel.setText(SourceFile.getName());
    }
    
    @FXML
    private void handleOpenDirectoryButton()
    {
        if(Desktop.isDesktopSupported())
        {
            try {
                Desktop.getDesktop().open(new File(OutputFilesPath));
            } catch (IOException e) {
                System.err.println(e);
            }
            
        }
    }
    
    @FXML
    public void handleReplaceAddressButton()
    {
        try {
            String source = FileToString(SourceFile.getPath());
            TemplateAddressCreator templateAdressCreator = new TemplateAddressCreator();
            templateAdressCreator.setBaseAddress(0);
            ParamSearcher addresSearcher = new ParamSearcher(source);
            Map<String,Param> TemplateAddresses = addresSearcher.SearchParams(templateAdressCreator);
            
            int minAddress = findMinInMap(TemplateAddresses);
            System.err.println(minAddress);
            templateAdressCreator.setBaseAddress(minAddress);
            addresSearcher = new ParamSearcher(source);
            TemplateAddresses = addresSearcher.SearchParams(templateAdressCreator);
            
            ParamReplacer paramReplacer = new ParamReplacer(source, TemplateAddresses);
            String out = paramReplacer.ReplaceParams();
            System.out.println(out);
        } catch (IOException e) {
            System.out.println(e);
        }
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Params = new HashMap<String, Param>();
        initializeTemplateCheckBox();
        initilizeParamsListView();
    }    
        
    public String[] getTemplates()
    {
        File templateDir = new File(TemplatesPath);
        return templateDir.list();
    }
    
    static String FileToString(String path) throws IOException
    {
        File file = new File(path);
        FileInputStream inp = new FileInputStream(file);
        InputStreamReader inpReader = new InputStreamReader(inp, "Cp1251");
        String text = "";
        try {
            BufferedReader br = new BufferedReader(inpReader);
            for(String line; (line = br.readLine()) != null; ) {
                text+=line+"\n";
            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }
        return text;
    }

    private void initializeTemplateCheckBox() {
        String[] files = getTemplates();
        templateChoiseBox.setItems(FXCollections.observableArrayList(files));
    }

    private String[] getMapStringKeys(Map<String, Param> map) {
        int i = 0;
        String[] keys = new String[map.size()];
        for(Map.Entry<String, Param> entry : map.entrySet()) {
            String key  = entry.getKey();
            keys[i] = key;
            i++;
        }
        return keys;
    }

    private void initilizeParamsListView() {
        paramsListView.setCellFactory(new Callback<ListView<String>, 
            ListCell<String>>() {
                @Override 
                public ListCell<String> call(ListView<String> list) {
                    return new ParamCell();
                }
        });
    }

    private void setAttributesValues() {
        Scene scene = paramsListView.getScene();
        ObservableList list = paramsListView.getItems();
        list.forEach((item) -> {
            TextField txtField = (TextField) scene.lookup("#"+item);
            Attributes.get(item).setValue(txtField.getText());
        });
    }

    private int getBaseAddress() {
        String baseAddressString = baseAddressField.getText();
        try{
            int baseAddress = Integer.parseInt(baseAddressString);
            return baseAddress;
        }catch(NumberFormatException e)
        {
            System.err.println(e);
            return 0;
        }
    }
    
    private boolean validateAddress(){
        String baseAddressString = baseAddressField.getText();
        try{
            int baseAddress = Integer.parseInt(baseAddressString);
            return true;
        }catch(NumberFormatException e)
        {
            System.err.println(e);
            
            return false;
        }
    }

    private int outputInFile(String txt) {
        String outputFileName = outputFileNameField.getText();
        if(outputFileName.isEmpty())
            return 2;
        File file = new File(OutputFilesPath + outputFileName);
        try {
            FileOutputStream fout = new FileOutputStream(file);
            OutputStreamWriter fwr = new OutputStreamWriter(fout, "Cp1251");
            //FileWriter fwr =  new FileWriter(outFileDelta, false);
            fwr.write(txt);
            fwr.close();
            return 1;
        } catch (FileNotFoundException e) {
            System.err.println(e);
            return -1;
        } catch (UnsupportedEncodingException e){
            System.err.println(e);
        } catch (IOException e){
            System.err.println(e);
        }
        return 0;
    }

    private int findMinInMap(Map<String, Param> map) {
        String[] keys = getMapStringKeys(map);
        int min = Integer.MAX_VALUE;
        for(String key: keys)
        {
            Param param = map.get(key);
            System.err.println();
            int val = TemplateAddress.getIntValue(param);
            if(val<min) min = val;
        }
        return min;
    }

    
}
