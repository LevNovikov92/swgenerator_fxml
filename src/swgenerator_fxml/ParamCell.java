/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swgenerator_fxml;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

/**
 *
 * @author NovikovLU
 */
public class ParamCell extends ListCell<String> {
        
    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        TextField textField = new TextField();
        textField.setId(item);
        textField.setLayoutX(100);
        Label label = new Label();
        label.setText(item);
        label.setId("label_"+item);
        label.setLayoutY(5);
        Pane pane = new Pane();
        pane.getChildren().addAll(textField, label);
        if (item != null) {
            setGraphic(pane);
        }
    }
}
