/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OPCTreeHelper;

/**
 *
 * @author Lev
 */
public abstract class ParamCreator {
    public abstract Param create(String name);
}
