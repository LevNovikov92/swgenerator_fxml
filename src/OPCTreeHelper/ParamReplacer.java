/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OPCTreeHelper;

import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Lev
 */
public class ParamReplacer {
    public String Source;
    public Scanner Scanner;
    public Map <String, Param> Params;
    
    public ParamReplacer(String source, Map <String, Param> Params)
    {
        this.Source = source;
        this.Scanner = new Scanner(source);
        this.Params = Params;
    }
    
    public String ReplaceParams() 
    {
        System.out.println("Replacing...");
        Scanner = new Scanner(Source);
        String txt = "";
        while(Scanner.hasNext())
        {
            String line = Scanner.nextLine();
            String newLine = line;
            for(Map.Entry<String, Param> entry : Params.entrySet()) {
                newLine = newLine.replaceAll(entry.getKey(), entry.getValue().getValue());
            }
            txt+= newLine + "\r\n";
        }
        return txt;
    }
}
