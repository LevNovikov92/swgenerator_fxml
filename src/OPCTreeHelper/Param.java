/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OPCTreeHelper;

/**
 *
 * @author Lev
 */
public abstract class Param {
    public static String Prefix = "";
    public static String Postfix = "";
    public String Name = "";
    public String Value = "";
    
    public abstract String getValue();
    
    public abstract void setValue(String value);
    
    public abstract String getPrefix();
    
    public abstract String getPostfix();
    
}
