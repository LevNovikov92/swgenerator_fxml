/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OPCTreeHelper;

import java.util.Hashtable;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Lev
 */
public class ParamSearcher {
    public String Source;
    public Scanner Scanner;
    public Map <String, Param> Params;
    
    public ParamSearcher(String source)
    {
        this.Source = source;
        this.Scanner = new Scanner(source);
    }
    
    public Map <String, Param> SearchParams(ParamCreator paramCreator)
    {
        Param param = paramCreator.create("_");
        String prefix = param.getPrefix();
        String postfix = param.getPostfix();
        Map <String, Param> Params = new Hashtable <String, Param>();
        System.out.println("Searching params...");
        while(Scanner.hasNext())
        {
            int indexPrefix = 0;
            int indexPostfix = 0;
            String line = Scanner.nextLine();
            while(line.indexOf(prefix, indexPrefix)!=-1)
            {   
                indexPrefix = line.indexOf(prefix, indexPrefix);
                if(line.indexOf(postfix, indexPrefix+prefix.length())!=-1)
                {
                    indexPostfix = line.indexOf(postfix, indexPrefix+prefix.length());
                    String attr = line.substring(indexPrefix, indexPostfix+postfix.length());
                    if(!Params.containsKey(attr))
                    {
                        Param Param = paramCreator.create(attr);
                        Params.put(attr, Param);
                    }
                }
                indexPrefix = indexPostfix;
                indexPostfix = indexPostfix + postfix.length();
            }
        }
        return Params;
    }
    
}
