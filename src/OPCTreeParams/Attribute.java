/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OPCTreeParams;

import OPCTreeHelper.Param;


/**
 *
 * @author Lev
 */
public class Attribute extends Param{
    public static String Prefix = "<<<";
    public static String Postfix = ">>>";
    public String Name;
    public String Value = "";
    
    public Attribute(String name)
    {
        Name = name;
    }
    @Override
    public String getValue()
    {
        return this.Value;
    }
    
    @Override
    public void setValue(String value)
    {
        Value = value;
    }

    @Override
    public String getPrefix() {
        return Prefix;
    }

    @Override
    public String getPostfix() {
        return Postfix;
    }
}
