/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OPCTreeParams;
import OPCTreeHelper.Param;
import OPCTreeHelper.ParamCreator;

/**
 *
 * @author NovikovLU
 */
public class TemplateAddressCreator extends ParamCreator{

    public int BaseAddress = 0;
    
    @Override
    public Param create(String name) {
        TemplateAddress.BaseAddress = BaseAddress;
        return new TemplateAddress(name);
    }
    
    public void setBaseAddress(int baseAddress)
    {
        BaseAddress = baseAddress;
    }
    
}
