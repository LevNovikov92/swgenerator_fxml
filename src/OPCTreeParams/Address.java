/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OPCTreeParams;

import OPCTreeHelper.Param;

/**
 *
 * @author Lev
 */
public class Address extends Param{

    public static int BaseAddress = 1;
    public static String Prefix = "Address=\"";
    public static String Postfix = "\"";
    public String Name;
    public String Value = "";
    
    public Address(String name)
    {
        Name = name;
        int prefixIndex = Name.indexOf(Prefix) + Prefix.length();
        int postfixIndex = Name.indexOf(Postfix, prefixIndex);
        if(prefixIndex!=-1 && postfixIndex!=-1)
            Value = Name.substring(prefixIndex, postfixIndex);
        
    }
    
    @Override
    public String getValue()
    {
        int address = BaseAddress + Integer.parseInt(Value);
        return Prefix + address + Postfix;
    }
    
    @Override
    public void setValue(String value)
    {
        Value = value;
    }

    @Override
    public String getPrefix() {
        return Prefix;
    }

    @Override
    public String getPostfix() {
        return Postfix;
    }
    
}
