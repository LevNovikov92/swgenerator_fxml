/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OPCTreeParams;
import OPCTreeHelper.Param;
/**
 *
 * @author NovikovLU
 */
public class TemplateAddress extends Address{

    public TemplateAddress(String name) {
        super(name);
        Postfix = "\"";
    }
    
    @Override
    public String getValue(){
        int address = Integer.parseInt(Value) - BaseAddress;
        return Prefix + address + Postfix;
    }
    
    public static int getIntValue(Param templateAddress){
        String stringValue = templateAddress.getValue();
        int indexPrefix = 0;
        int indexPostfix = stringValue.indexOf(Postfix, Prefix.length());
        stringValue = stringValue.substring(Prefix.length(), indexPostfix);
        int value = Integer.parseInt(stringValue);
        return value;
    }
    

}
