/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OPCTreeParams;

import OPCTreeHelper.Param;
import OPCTreeHelper.ParamCreator;

/**
 *
 * @author Lev
 */
public class AddressCreator extends ParamCreator{

    public int BaseAddress = 0;
    
    @Override
    public Param create(String name) {
        Address.BaseAddress = BaseAddress;
        Address address = new Address(name);
        return new Address(name);
    }
    
    public void setBaseAddress(int baseAddress)
    {
        BaseAddress = baseAddress;
    }
}
