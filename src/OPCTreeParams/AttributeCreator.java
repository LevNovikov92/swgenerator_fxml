/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OPCTreeParams;

import OPCTreeHelper.Param;
import OPCTreeHelper.ParamCreator;

/**
 *
 * @author Lev
 */
public class AttributeCreator extends ParamCreator{

    @Override
    public Param create(String name) {
        return new Attribute(name);
    }
}
